=   Warum Mathe?
Achim Domma
:sectnums:
:toc: left
:toclovels: 2
:doctype: book

== Mathe ist blöd!?

Mathematik. Als Schulfach wird sie von wenigen geliebt und von vielen gehasst.
Da du auf dieser Seite gelandet bist, gehörst du wahrscheinlich zur letzteren
Gruppe. Falls ja, habe ich Angebot für dich:

Warum sollte man sich überhaupt mit Mathe beschäftigen. Fällt dir etwas ein?
"Weil meine Lehrer*in mich zwingt!" zählt nicht. Ja klar, das stimmt schon. Aber die
Frage ist: Wofür ist Mathe gut? ... Na? Hast du 'ne Idee?

Nein? Könnte das der Grund dafür sein, dass du Mathe doof findest? Weil alles
so sinnlos ist? Weil dir niemand je gesagt hat, was der ganze Quatsch soll?

Falls ich Recht habe, ist hier mein Angebot: Du liest einfach ein bisschen
weiter. Ich erkläre dir worum es bei Mathematik eigentlich geht. Du musst kein
Fan von Mathe werden. Aber vielleicht fällt dir Mathe in der Schule
anschließend ein bisschen leichter. Das würde mich freuen. Dann hätte ich
gezeigt, dass Mathe gar nicht soooo blöd ist. Ist Mathe nämlich nicht. Blöd
sind Schule und deine Unterricht. ;-)

== Kleinkinder, Zahlen und Sprache

Wusstest du, dass du Mathe mal ziemlich spannend fandest? Doch, wirklich!
Eigentlich alle Kleinkinder fangen irgendwann an zu zählen. Sie verstehen, dass
man nach Größe, Anzahl, ... sortieren kann. Oder finden vollkommen selbständig
heraus, dass man, wenn man schon zwei hat und noch zwei dazu tut, vier hat.
Meistens finden sie das super spannend und können sich ewig damit beschäftigen.
Zumindest so lange Erwachsene ihnen nicht mit dummen Fragen den Spass
verderben.

Dabei stellen sie oft schon schlaue Fragen, die Erwachsene überfordern, weil 
diese selbst zu wenig Ahnung haben. Schau mal folgendes Bild an:

 hier kommt ein Bild mit vier Punkten hin

Warum sagen wir "Das sind vier Punkte"? Was wäre denn, wenn wir dazu einfach
"fünf Punkte" sagen würden? Kinder fragen das überraschend oft. Wollen wir die
Antwort zusammen herausfinden?

Schau' die Punkte mal fest an und sag' laut und deutlich "fünf Punkte!". Ändert
sich etwas? Die Anzahl der Punkte bleibt natürlich gleich. Du fragst dich jetzt
vermutlich, ob es wirklich sinnvoll ist, weiterzulesen. Und ob ich evtl.
spinne. Das ist ok, aber das war gerade ein ziemlich wichtiger Punkt:

Das Wort "vier" ist nur eine Bezeichnung, auf die wir uns "geeinigt" haben,
damit wir darüber reden können. Darüber denkt man selten nach, aber eigentlich
ist "vier" eine ungewöhnliche Bezeichnung für die Anzahl der Punkte oben.
Glaubst du nicht? Engländer*innen sagen z.B. "four" dazu. Chines*innen oder
Inder*innen verwenden nochmal andere Worte. Aber wir meinen alle das gleiche.

Jetzt denk mal an deine Eltern, wenn du dich mit guten Freund*innen
unterhältst. Z.B. über eure Zockerei oder ein anderes Thema, von dem deine
Eltern keinen Plan haben. Ihr verwendet Fachbegriffe, die euch absolut klar
sind. Das macht die Kommunikation kürzer, knapper und auf den Punkt. Deine
Eltern werden aber nur verständnislos schauen. Selbst wenn du ihnen die
Begriffe erklärst, wird das wenig nützen. Ohne das Thema zu verstehen,
helfen auch die Begriffe nichts.

Genau das ist dir mit Mathe in der Schule passiert. Mathelehrer*innen zocken
mit Zahlen und du stehst - planlos wie deine Eltern - daneben und verstehst
nicht mal, worum es in dem Spiel geht. Und genau das werden wir jetzt ändern.


== Probleme & Rätsel

Mathe ist eigentlich ein Werkzeug, um Probleme zu lösen. Und sich dabei die
Arbeit möglichst leicht zu machen. Ja, Mathe ist ein Werkzeug um faul zu sein.

Natürlich können wir hier nicht die Konstruktion einer Rakete zum Mond
durchrechnen. Laß uns mit einem Rätsel starten, das zumindest grob einen Sinn
ergibt:

[quote]
"Bei uns ist unsere Freundin Lisa. Lisa und ich haben jeweils eine Hand
geschlossen. In der Hand halten wir Münzen. Würde mir Lisa eine ihrer Münzen
abgeben, hätte wir gleich viele Münzen. Würde ich Lisa eine meiner Münzen
geben, hätte sie doppelt so viele Münzen wie ich. +
 +
Wenn du magst, darfst du raten, wieviele Münzen wir in den Händen haben.
Stimmt deine Antwort, bekommst du die Münzen. Liegst du falsch, mußt du
uns die gleiche Anzahl Münzen geben."

Bevor du dich entscheidest, denkst du natürlich erstmal nach. Wenn du die
Antwort sicher weißt, solltest du dir die Münzen nicht entgehen lassen. Aber
vielleicht wollen wir dich auch über den Tisch ziehen?!

=== Raten und Probieren

Wie könnte man das Problem angehen? Die erste Option ist immer: Raten! Also
versuchst du es mit "Lisa: 2" und "Achim/Ich: 6". Ob das die Lösung ist, kannst
du leicht ausprobieren: Wenn Lisa mir eine ihrer Münzen gibt, hat sie noch eine
Münze und ich hätte sieben. Eigentlich sollten wir in dem Fall aber gleichviele
Münzen haben. "2 und 6" ist also schonmal keine Lösung.

Du kannst gerne noch ein paar mal raten, aber es gibt ziemlich viele denkbare
Möglichkeiten. Das könnte dauern. Gibt's einen besseren Weg? Natürlich. Sogar
viele verschiedene. Aber laß uns mal rückwärts an die Sache gehen:

Wenn Lisa mir eine Münze gibt, haben wir anschließend gleich viele Münzen.
"Gleich viele" ist noch keine Zahl, aber besser als nichts: Wir haben also
beide eine Münze oder beide zwei Münzen oder beide drei Münzen ... Immer noch
viele Möglichkeiten, aber schon deutlich weniger!

Wenn dir beide eine Münzen haben, nachdem mir Lisa eine von ihren gegeben hat,
dann hatte sie vorher eine mehr. Also zwei Münzen. Und ich hatte eine weniger.
Also gar keine Münze. Logisch, oder? Laß uns das mal für andere Zahlen
aufschreiben:

[cols="1,1,1"]
|===
|Beide|Lisa vorher|Achim vorher

|1
|2
|0

|2
|3
|1

|3
|4
|2

|4
|5
|3

|5
|6
|4

|6
|7
|5

|...
|...
|...
|===

So aufgeschrieben siehst du sicher direkt das Muster: Lisa muß zwei Münzen mehr
haben, damit folgendes stimmt:


[quote]
"Würde mir Lisa eine ihrer Münzen abgeben, hätte wir gleich viele Münzen."

Als nächstes kümmern wir uns um

[quote]
Würde ich Lisa eine meiner Münzen geben, hätte sie doppelt so viele Münzen wie ich.

Wir wissen ja von oben, welche "Lisa vorher" und "Achim vorher" Möglichkeiten
überhaupt nur in Frage kommen. Also schauen wir uns an, was bei denen passiert
wenn dieses mal ich eine meiner Münzen an Lisa gebe:

[cols="1,1,1,1"]
|===
|Lisa vorher|Achim vorher|Lisa nachher|Achim nachher

|2
|0
|geht
|nicht

|3
|1
|4
|0

|4
|2
|5
|1

|5
|3
|6
|2

|6
|4
|7
|3

|7
|5
|8
|4

|8
|6
|9
|5

|...
|...
|...
|===

Jetzt schauen wir einfach, ob die zwei letzten Spalten in einer der Reihen
passen. Die erste Zeile scheidet schonmal aus. Wenn ich keine Münze habe, kann
ich keine an Lisa abgeben.
In der nächsten Zeile hätte Lisa vier Münzen und ich keine. Vier ist
offensichtlich nicht doppelt so viel wie Null. Also geht's weiter mit der
nächsten Zeile. Fünf ist nicht doppelt so viel wie eins. Und sechs nicht
doppelt soviel wie zwei. Auch sieben ist nicht das Doppelte von 3, aber ...
tadaa ... acht ist doppelt so viel wie vier!

Sieht aus, als hätten wir eine Lösung!? Kurz durchatmen und nochmal genau prüfen:

Laut der "vorher"-Spalten würde Lisa also sieben Münzen in der Hand halten und
ich fünf. Gibt Lisa mir eine vor ihren Münzen, hätten wir beide sechs.
Gleich viel, so wie es sein soll. Gebe ich Lisa eine Münze, habe ich nur noch
vier und sie acht. Doppelt so viele wie ich. Auch das ist korrekt.

Du zeigst mit den Worten "Sieben!" auf Lisa und mit "Fünf!" auf mich. Wir
öffenen die Hände und du streichst jubelnd die Münzen ein!

=== Bequemlichkeit

Uff! Das war ganz schön viel Text für so ein kleines Rätsel. Und wenn wir
ehrlich sind war schon auch Glück im Spiel: Du hast die passende Lösung recht
am Anfang der Zahlenreihen gefunden. Bei anderen ähnlichen Rätseln müsstest du
vermutlich viel länger probieren.

// == Kapitel 1

// include::chapter1.adoc[]

// == Kapitel 2

// include::chapter2.adoc[]
